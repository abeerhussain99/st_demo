import 'dart:math' as math;

import 'package:flutter_test/flutter_test.dart';

void main() {
  test("Demo test for ST app", () {
    int result = math.max<int>(2, 4);

    expect(result, 4);
  });
}
